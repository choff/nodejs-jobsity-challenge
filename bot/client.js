const amqp = require("amqplib/callback_api");

/*
 * This module receive a stock code as parameter and call to remote worker to
 * get info about the stock code, it works with rabbitMQ and a RPC pattern
 * considering this module as the client or the request in req/req pattern
 */

const get_stock_code = function(item, cb) {
  console.log("asking to bot for ", item);
  amqp.connect(process.env.AMQP_URL, function(err0, conn) {
    if (err0) console.log("senderJS err0:", err0);

    conn.createChannel(function(err1, channel) {
      if (err1) cb(err1);

      channel.assertQueue("", { exclusive: true }, function(err2, q) {
        if (err2) cb(err2);
        let correlationId = CreateUUID();

        // consume answer from RPC worker
        channel.consume(
          q.queue,
          function(msg) {
            if (msg.properties.correlationId === correlationId) {
              let message = JSON.parse(msg.content.toString());
              console.log(" [x] client rec:", message);
              let err = message.error ? message.error : null;
              setTimeout(function() {
                conn.close();
                cb(err, message.msg);
              }, 500);
            }
          },
          { noAck: true },
        );
        let data = { cmd: "/stock", item };
        //console.log("client bot item is:", item);

        // send data to worker
        channel.sendToQueue(
          "rpc_queue",
          new Buffer.from(JSON.stringify(data)),
          {
            correlationId: correlationId,
            replyTo: q.queue,
          },
        );
      });
    });
  });
};

/*
 * Create UUID for correlation ID in socket messages
 */

function CreateUUID() {
  return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(c) {
    let r = (Math.random() * 16) | 0,
      v = c == "x" ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
}

module.exports = {
  get_stock_code,
};
