const request = require("request");
const amqp = require("amqplib/callback_api");
require("dotenv").config();

/*
 * This module receive a stock code from a client and call to remote API to
 * get csv info about the stock code, it process the csv info and send it to
 * client througth rabbitmq
 */

amqp.connect(process.env.AMQP_URL, function(err0, conn) {
  if (err0) console.log("senderJS err0:", err0);

  // create Channel
  conn.createChannel(function(err1, channel) {
    if (err1) console.log("senderJS err1:", err1);
    let queue = "rpc_queue";

    channel.assertQueue(queue, { durable: false });

    channel.prefetch(1);
    console.log(" [x] Awaiting stock_codes requests");

    // consume queue messages
    channel.consume(queue, function reply(msg) {
      let message = JSON.parse(msg.content.toString());
      console.log("  [x] incomming message ", message);
      if (message.cmd === "/stock") {
        let url = `https://stooq.com/q/l/?s=${message.item}&f=sd2t2ohlcv&h&e=csv`;

        // get stock_code info from remote api
        request(url, function(err, res, body) {
          if (!err) {
            let data = body.split("\r\n")[1].split(",");
            let price = data[3];
            let ans = "";
            if (price === "N/D") {
              ans = { error: true, msg: `stock ${data[0]} not found` };
            } else {
              ans = { msg: `${data[0]} quote is $${data[3]} per share` };
            }

            // reply to client
            channel.sendToQueue(
              msg.properties.replyTo,
              new Buffer.from(JSON.stringify(ans)),
              {
                correlationId: msg.properties.correlationId,
              },
            );
            channel.ack(msg);
          }
        });
      }
    });
  });
});
