const express = require("express");
const router = express.Router();
const { ensureAuthenticated } = require("../config/auth");

/* initial page */
router.get("/", function(req, res, next) {
  res.render("index", { testing: "work in progress.." });
});

// Dashboard
router.get("/dashboard", ensureAuthenticated, (req, res) => {
  res.render("dashboard", {
    user: req.user,
  });
});

module.exports = router;
