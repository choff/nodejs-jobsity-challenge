const express = require("express");
const bcrypt = require("bcryptjs");
const passport = require("passport");
const router = express.Router();

// User model
const User = require("./../models/User");

// Login page
router.get("/login", (req, res) => {
  res.render("login");
});

// Register page
router.get("/register", (req, res) => {
  res.render("register");
});

// handel new register user
router.post("/register", (req, res) => {
  console.log("/register");
  const { name, email, password, password2 } = req.body;
  let errors = [];

  // Check required fields
  if (!name || !email || !password || !password2) {
    errors.push({ msg: "Please fill in all fields" });
  }

  // check password match
  if (password !== password2) {
    errors.push({ msg: "Password do not match" });
  }

  // check pass length
  if (password.length < 6) {
    errors.push({ msg: "Password should be at least 6 characters" });
  }

  if (errors.length > 0) {
    res.render("register", {
      errors,
      name,
      email,
      password,
      password2,
    });
  } else {
    // Validation passed
    User.findOne({ email: email })
      .then(user => {
        // user exist
        errors.push({ msg: "Email is already registered" });
        if (user) {
          res.render("register", {
            errors,
            name,
            email,
            password,
            password2,
          });
        } else {
          // create user
          const newUser = new User({
            name,
            email,
            password,
            password2,
          });

          // Hash pass
          bcrypt.genSalt(10, (err, salt) =>
            bcrypt.hash(newUser.password, salt, (err, hash) => {
              if (err) throw err;
              // set pass to hashed
              newUser.password = hash;
              // save user
              newUser
                .save()
                .then(user => {
                  req.flash("success_msg", "You are now registered!!");
                  console.log("success");
                  res.redirect("/users/login");
                })
                .catch(err => console.log(err));
            }),
          );
        }
      })
      .catch();
  }
});

// login handler
router.post("/login", (req, res, next) => {
  passport.authenticate("local", {
    successRedirect: "/dashboard",
    failureRedirect: "/users/login",
    failureFlash: true,
  })(req, res, next);
});

// logout handler
router.get("/logout", (req, res) => {
  req.logout();
  req.flash("success_msg", "You are log out");
  res.redirect("/users/login");
});

module.exports = router;
