/* Message model */

const mongoose = require("mongoose");

const MessageSchema = new mongoose.Schema({
  // email of message author
  user: {
    type: String,
    required: true,
  },
  message: {
    type: String,
    required: true,
  },
  date: {
    type: Date,
    default: Date.now,
  },
  room: {
    type: String,
    default: "default",
  },
});

const Message = mongoose.model("Message", MessageSchema);

module.exports = Message;
