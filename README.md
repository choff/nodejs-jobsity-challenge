# nodejs-Jobsity-challenge

NodeJs chat rooms application with deacoupled bot!!!

## installation
``` bash
$ git clone https://gitlab.com/choff/nodejs-jobsity-challenge.git
$ cd nodejs-jobsity-challenge.git
$ npm install
```

## Execution
1. Fisrt run the bot as follows: 
```bash
node bot/worker.js
```

2. run the chat server:
```bash
npm start
```
3. Open in a web browser [http://localhost:5000](http://localhost:5000)
   and register/login
   
## Configuration

For now in config/config.js there are just one variable to configure  and it
corresponds to the number of messages to display in chat, by default it is 50

The application use the variables *mongoURI* and *AMQP_URL* to connet with mongo
database and rabbitMQ server, i have added the file .env with that variables just
for easy replication.

## Bonus

- Multiple rooms


## Notes

- when an user in room ask to bot for info about one stock the server reply
  the bot answer to all users in room, if you want that the server just reply
  to the user that sent the command, just comment line 57 in socket/eventHandler.js


