/*
 *  Module that handle messages in socket
 *
 */

// Message model
const Msg = require("../models/Message");

// socket message handler
const handle = require("./eventHandler");

// rabbitMQ client
const rpc_client = require("../bot/client");

// rooms by default
let rooms = ["default", "nodejs", "rock"];
let users = {};

module.exports = function(server) {
  const io = require("socket.io")(server);

  io.on("connection", socket => {
    // send registration message to client
    socket.emit("register", { rooms });

    // handle new-user message
    handle.new_user_event(socket, users);

    // handle send-chat-message
    handle.chat_message_event(socket, rpc_client);

    // handle new-room message
    handle.new_room_event(socket, rooms);

    // handel change-room message
    handle.change_room_event(socket, rooms);

    // handle disconnect event
    socket.on("disconnect", () => {
      console.log(`disconnect: ${users[socket.id]}`);
      socket.broadcast.emit("user-disconnected", users[socket.id]);
      delete users[socket.id];
    });
  });
};
