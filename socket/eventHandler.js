/*
 * This module handle all messages in socket
 */

const Msg = require("../models/Message");
const cfg = require("../config/config");

/* handle new-user message.
 * add user to inmemory users object, join it to default room
 * and broadcast to default room the new user
 */

const new_user_event = function(socket, users) {
  socket.on("new-user", data => {
    // console.log("new-user", data);
    Msg.find(
      {},
      {},
      { sort: { date: -1 }, limit: cfg.last_n_message },
      (err, regs) => {
        if (err) console.log(err);
        users[socket.id] = data.user;
        socket.join("default");
        socket.broadcast.to("default").emit("user-connected", data.user);
        socket.emit("history", regs.reverse());
      },
    );
  });
};

/* handle send-chat-message event. search in the message to distribute,
 * if it start with "/stoc=" command call the remote procedure to get info
 * and broadcast that info to chanel.
 */
const chat_message_event = function(socket, rpc_client) {
  socket.on("send-chat-message", message => {
    // console.log(`chat-message:`, message);
    let msg = message.message;

    // if start with "/stock" process the code
    if (msg.startsWith("/stock=")) {
      if (msg.length > 7 && msg.substring(7).replace(/\s/g, "").length > 0) {
        let cmd = msg.substring(0, 6);
        let item = msg.substring(7).replace(/\s/g, "");
        rpc_client.get_stock_code(item, function(err, ans) {
          let tmp = {
            date: new Date(),
            user: "bot@jobsity.com",
            message: ans,
          };
          if (err) socket.emit("bad-command", tmp);
          else {
            socket.emit("chat-message", tmp);

            // comment this if you want to send answer
            // just to the client that send petition.
            socket.broadcast.to(message.room).emit("chat-message", tmp);
          }
        });
      } else {
        // do not distribute message, reply to sender that there are not args
        socket.emit("bad-command", {
          date: new Date(),
          message: "please add stock code after '/stock=' ",
        });
      }
    } else {
      // if message is not a command save and broadcast it
      const newMsg = new Msg(message);
      newMsg
        .save()
        .then(data => {
          socket.broadcast.to(data.room).emit("chat-message", data);
        })
        .catch(err => console.log(err));
    }
  });
};

/*
 * handle the new-room event that occurs when a new room it is created by user
 */

const new_room_event = function(socket, rooms) {
  socket.on("new-room", roomname => {
    if (rooms.includes(roomname)) {
      console.log(`room ${roomname} already exist`);
    } else {
      rooms.push(roomname);
      socket.broadcast.emit("add-room", roomname);
    }
  });
};

/*
 * handle change-room message received from a client that wants see another room
 */

const change_room_event = function(socket, rooms) {
  socket.on("change-room", data => {
    //console.log("change room:", data);
    socket.leave(data.from);
    if (rooms.includes(data.to)) {
      socket.join(data.to);

      // after join to run get last n messages in that room
      Msg.find(
        { room: data.to },
        {},
        { sort: { date: -1 }, limit: cfg.last_n_message },
        (err, regs) => {
          if (err) console.log(err);
          socket.to(data.to).broadcast.emit("user-connected", data.user);
          socket.emit("history", regs.reverse());
        },
      );
    } else {
      console.log(`room ${data.to} does not exit`);
    }
  });
};

module.exports = {
  new_user_event,
  chat_message_event,
  new_room_event,
  change_room_event,
};
