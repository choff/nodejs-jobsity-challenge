const express = require("express");
const expressLayouts = require("express-ejs-layouts");
const flash = require("connect-flash");
const session = require("express-session");
const mongoose = require("mongoose");
const passport = require("passport");
require("dotenv").config();

const app = express();

// Passport config
require("./config/passport")(passport);

// Conect to Mongo
const db = process.env.mongoURI;
mongoose
  .connect(db, { useNewUrlParser: true })
  .then(() => console.log("MongoDB connected..."))
  .catch(err => console.log(err));

// EJS
app.set("view engine", "ejs");
app.use(expressLayouts);
app.use(express.static(__dirname + "/public"));

// Bodyparser
app.use(express.urlencoded({ extended: false }));

// Express sessions
app.use(
  session({
    secret: "share is good",
    resave: true,
    saveUninitialized: true,
  }),
);

// passport middleware
app.use(passport.initialize());
app.use(passport.session());

// connect flash
app.use(flash());

// Global vars
app.use((req, res, next) => {
  res.locals.success_msg = req.flash("success_msg");
  res.locals.error_msg = req.flash("error_msg");
  res.locals.error = req.flash("error");
  next();
});

// handle routes
app.use("/", require("./routes/index"));
app.use("/users", require("./routes/users"));

// Create Server
const PORT = process.env.PORT || 5000;
const server = app.listen(PORT, () => {
  console.log(`Server started at ${process.env.HOST}:${PORT}`);
});
server.on("error", onError);

// socket logic here!!
require("./socket/index")(server);

// Event listener for HTTP server "error" event.
function onError(error) {
  if (error.syscall !== "listen") throw error;

  let bind = typeof PORT === "string" ? "Pipe " + PORT : "Port " + PORT;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case "EACCES":
      console.error(bind + " requires elevated privileges");
      process.exit(1);
      break;
    case "EADDRINUSE":
      console.error(bind + " is already in use");
      process.exit(1);
      break;
    default:
      throw error;
  }
}
