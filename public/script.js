/*
 * Socket Io Client
 * handle socket communication with server and user events in forms
 */

const socket = io();
const messageForm = document.getElementById("send-container");
const messageInput = document.getElementById("message-input");
const messageContainer = document.getElementById("message-container");
const roomsContainer = document.getElementById("rooms-container");
const roomsForm = document.getElementById("rooms-form");
const roomInput = document.getElementById("roomName");

let user = document.getElementById("username").innerText;
let rooms = [];
let activeRoom = "default";

// send user email (id) to server to register in default room
socket.on("register", msg => {
  console.log("register rooms:", msg);
  msg.rooms.forEach(room => {
    if (!rooms.includes(room)) {
      rooms.push(room);
      appendRooms(room);
    }
  });
  document.getElementById(activeRoom).style.backgroundColor = "red";
  socket.emit("new-user", { user });
});

// handle info about new users in chat
socket.on("user-connected", username => {
  console.log("user-connected:", username);
  let name = username.substring(0, username.indexOf("@"));
  appendMessage(`${name} joined`);
});

// handle user disconnected event
socket.on("user-disconnected", username => {
  appendMessage(`${username} disconnected!`);
});

// receive last chats in room
socket.on("history", data => {
  console.log("history:", data);
  data.forEach(msg => {
    let time = new Date(msg.date);
    let tmp = `${time.getHours()}:${time.getMinutes()}`;
    let username = msg.user.substring(0, msg.user.indexOf("@"));
    appendMessage(`${tmp} - ${username}: ${msg.message}`);
  });
});

// handle incomming new chat messages
socket.on("chat-message", data => {
  console.log("chat-message:", data);
  let time = new Date(data.date);
  let tmp = `${time.getHours()}:${time.getMinutes()}`;
  const username = data.user.substring(0, data.user.indexOf("@"));
  appendMessage(`${tmp} - ${username}: ${data.message}`);
});

// handle wrong command answer from bot
socket.on("bad-command", data => {
  console.log("bad-command:", data);
  let time = new Date(data.date);
  let tmp = `${time.getHours()}:${time.getMinutes()}`;
  appendMessage(`${tmp} - Bot: ${data.message}`);
});

// append new room in room list
socket.on("add-room", roomname => {
  if (!rooms.includes(roomname)) {
    rooms.push(roomname);
    appendRooms(roomname);
  }
});

// handle chat submit event in form
messageForm.addEventListener("submit", e => {
  e.preventDefault();
  const message = messageInput.value;
  const data = {
    room: activeRoom,
    user,
    message,
  };
  socket.emit("send-chat-message", data);

  if (!message.startsWith("/stock=")) {
    let time = new Date();
    let tmp = `${time.getHours()}:${time.getMinutes()}`;
    appendMessage(`${tmp} - You: ${message}`);
  }
  messageInput.value = "";
});

// handle new room submit
roomsForm.addEventListener("submit", e => {
  e.preventDefault();
  const roomname = roomInput.value;
  if (!rooms.includes(roomname)) {
    socket.emit("new-room", roomname);
    socket.emit("change-room", { from: activeRoom, to: roomname, user });
    appendRooms(roomname);
    rooms.push(roomname);
  } else {
    alert(`the room ${roomname} already exists!`);
  }
  roomInput.value = "";
});

// append room to room list
function appendRooms(room) {
  const roomElement = document.createElement("li");

  const a = document.createElement("a");
  const linkText = document.createTextNode(room);
  a.appendChild(linkText);
  a.href = "#";
  a.id = room;
  a.onclick = function(e) {
    let room = e.target.id;
    console.log(rooms);
    if (rooms.includes(room)) {
      clearMsgs();
      socket.emit("change-room", { from: activeRoom, to: room, user });
      document.getElementById(activeRoom).style.backgroundColor = "white";
      a.style.backgroundColor = "red";
      activeRoom = room;
    }
  };
  roomElement.appendChild(a);
  roomsContainer.append(roomElement);
}

// clear all messages
function clearMsgs() {
  let totalMsgs = messageContainer.childNodes.length;
  for (let i = 0; i < totalMsgs; ++i) {
    messageContainer.removeChild(messageContainer.childNodes[0]);
  }
}

// append message to messages session
function appendMessage(message) {
  let maxMsg = 10;
  let totalMsgs = messageContainer.childNodes.length;
  if (totalMsgs === maxMsg) {
    messageContainer.removeChild(messageContainer.childNodes[0]);
  }
  const messageElement = document.createElement("div");
  messageElement.innerText = message;
  messageContainer.append(messageElement);
}
